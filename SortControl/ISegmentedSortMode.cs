﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace SortControl
{
	public interface ISegmentedSortMode : ISortMode
	{
		int SegmentSize { get; set; }
	}
}
