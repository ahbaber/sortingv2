﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;

namespace SortControl
{
	public interface ISortController
	{
		int Setting { get; }
		bool UpdateSetting(int newSetting);
		ISortController AddSorter(ISortMode sorter);
		ISortController AddSorter(ISegmentedSortMode sorter);
		ISorter GetSorter();
	}
}
