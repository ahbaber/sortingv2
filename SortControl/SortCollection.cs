﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using System.Collections.Generic;
using TraceControl;

namespace SortControl
{
	public class SortCollection : ISortController
	{
		public SortCollection(ITraceControl traceControl)
		{
			_traceControl = traceControl;
		}

		public int Setting { get; private set; } = 0;

		public ISortController AddSorter(ISortMode sorter)
		{
			_modes.Add(sorter);
			return this;
		}

		public ISortController AddSorter(ISegmentedSortMode sorter)
		{
			_modes.Add(sorter);
			return this;
		}

		public bool UpdateSetting(int newSetting)
		{
			if (0 <= newSetting && newSetting < _modes.Count)
			{
				Setting = newSetting;
				return true;
			}
			return false;
		}

		public ISorter GetSorter()
		{
			return _modes[Setting].GetSorter(_traceControl.GetFactories());
		}

		private readonly List<ISortMode> _modes = new List<ISortMode>();
		private readonly ITraceControl _traceControl;
	}
}
