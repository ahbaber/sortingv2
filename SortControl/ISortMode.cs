﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using TraceControl.Factories;

namespace SortControl
{
	public interface ISortMode
	{
		string Name { get; }
		ISorter GetSorter(MetaFactory factories);
	}
}
