﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using TraceControl.Factories;

namespace SortControl.SortModes
{
	public class Simple : ISortMode
	{
		public string Name => "Simple";

		public ISorter GetSorter(MetaFactory factories)
		{
			return factories.SimpleSortFactory.Construct();
		}
	}
}
