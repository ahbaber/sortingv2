﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using TraceControl.Factories;

namespace SortControl.SortModes
{
	public class Quick : ISortMode
	{
		public string Name => "Quick";

		public ISorter GetSorter(MetaFactory factories)
		{
			return factories.QuickSortFactory.Construct();
		}
	}
}
