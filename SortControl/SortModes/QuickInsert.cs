﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using System;
using TraceControl.Factories;

namespace SortControl.SortModes
{
	public class QuickInsert : ISortMode, ISegmentedSortMode
	{
		public string Name => _name ?? GetName();

		public int SegmentSize
		{
			get { return _minSegment; }
			set { _minSegment = value; _name = null; }
		}

		public ISorter GetSorter(MetaFactory factories)
		{
			var sort = factories.QuickInsertSortFactory.Construct();
			sort.MinSegment = SegmentSize;
			return sort;
		}

		private int SetSegment(int value)
		{
			_name = null;
			_minSegment = Math.Max(value, 1);
			return value;
		}

		private string GetName()
		{
			_name = $"QuickInsert({_minSegment})";
			return _name;
		}

		private string _name;
		private int _minSegment = 8;
	}
}
