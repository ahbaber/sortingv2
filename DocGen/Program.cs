﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters;
using System;
using System.IO;

namespace DocGen
{
	class Program
	{
		static void Main()
		{
			var xmlSmallNodeFormatter = new XmlSmallNodeFormatter();
			GenerateCsv("XmlSmall", xmlSmallNodeFormatter);

			var htmlNodeFormatter = new HtmlNodeFormatter();
			GenerateCsv("HtmlFormatter", htmlNodeFormatter);
		}

		private static void GenerateCsv(string filename, IDocumented rootRule)
		{
			var docGen = new DocGenerator();
			var docs = docGen.FormatRules(rootRule);

			filename = GetFilePath(filename);

			using (var writer = new StreamWriter(filename, false))
			{
				foreach (var line in docs)
				{
					writer.Write(line);
				}
			}

			Console.WriteLine($"Wrote docs to: {filename}");
			Console.WriteLine($"Lines: {docs.Count}");
		}

		private static string GetFilePath(string filename)
		{
			string dir = Directory.GetCurrentDirectory();
			string projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
			int index = dir.IndexOf(projectName, StringComparison.InvariantCulture);
			dir = dir.Remove(index); // now at solution root folder

			dir = Path.Combine(dir, "Output");
			Directory.CreateDirectory(dir);

			return Path.Combine(dir, $"{filename}.csv");
		}
	}
}
