﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Xml
{
    public class DataSetSwap : INodeTaggedParser, IDocumented
    {
        public string Tag => "DataSet.Swap";

        public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
        {
            string i = node.Parameters["i"];
            string j = node.Parameters["j"];
            return builder.Append($"{indent}<{node.Tag} i=\"{i}\" j=\"{j}\" />{newLine}");
        }

        public RuleData CreateDocumentation()
        {
            return RuleData.Construct("DataSet Swap Parser", "Processes DataSet value swaps", this)
                .AddData("DataSet", Tag);
        }
    }
}
