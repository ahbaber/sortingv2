﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Xml
{
	public class SortsAndPartitions : AbstractNodeChildProcessor, INodeMultiTagParser, IDocumented
	{
		public List<string> Tags => _tags;

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			builder.Append($"{indent}<{node.Tag}>{newLine}");
			ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
			return builder.Append($"{indent}</{node.Tag}>{newLine}");
		}

		public RuleData CreateDocumentation()
		{
			return RuleData.Construct("Sorts and Partitions", "Processes multiple algorithms sorts and partitions", this)
				.AddData("QuickSort", "Partition Sort")
				.AddData("QuickInsertSort", "Partition Sort")
				.AddData("HeapSort", "Sort")
				.AddData("SimpleSort", "Sort")
				.AddData("SelectionSort", "Sort")
				.AddData("InsertSort", "Sort");
		}

		private static readonly List<string> _tags = new List<string>
		{
			"QuickSort.Sort",
			"QuickSort.Partition",
			"QuickInsertSort.Sort",
			"QuickInsertSort.Partition",
			"HeapSort.Sort",
			"SimpleSort.Sort",
			"SelectionSort.Sort",
			"InsertSort.Sort",
		};
	}
}
