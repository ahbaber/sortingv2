﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Xml
{
    public class QuickSortGetMedianValue : INodeMultiTagParser, IDocumented
    {
        public List<string> Tags => _tags;

        public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
        {
            string i = node.Parameters["i"];
            string j = node.Parameters["j"];
            string k = node.Parameters["k"];
            return builder.Append($"{indent}<{node.Tag} i=\"{i}\" j=\"{j}\" k=\"{k}\">{node.Value}</{node.Tag}>{newLine}");
        }

        public RuleData CreateDocumentation()
        {
            return RuleData.Construct("Median Value Parser", "Processes median values", this)
                .AddData("QuickSort", "GetMedianValue")
                .AddData("QuickInsertSort", "GetMedianValue");
        }

        private static readonly List<string> _tags = new List<string>
        {
            "QuickSort.GetMedianValue",
            "QuickInsertSort.GetMedianValue",
        };
    }
}
