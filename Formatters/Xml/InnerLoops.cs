﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using System.Text;
using Traceability;
using Traceability.Formatters;
using Traceability.Formatters.XmlSupport;

namespace Formatters.Xml
{
    public class InnerLoops : XmlNodeParser, INodeMultiTagParser, IDocumented
    {
        public List<string> Tags => _tags;

        public InnerLoops()
        : base(null, null)
        { }

        public override StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
        {
            string i = GetI(node);
            builder.Append($"{indent}<{node.Tag} i=\"{i}\">{newLine}");
            ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
            return builder.Append($"{indent}</{node.Tag}>{newLine}");
        }

        public RuleData CreateDocumentation()
        {
            return RuleData.Construct("InnerLoop Parser", "Processes the inner loop", this)
                .AddData("SimpleSort", "InnerLoop")
                .AddData("SelectionSort", "InnerLoop")
                .AddData("InsertSort", "InnerLoop");
        }

        private string GetI(TraceNode node)
        {
            return node.Parameters["i"];
        }

        private static readonly List<string> _tags = new List<string>
        {
            "SimpleSort.InnerLoop",
            "SelectionSort.InnerLoop",
            "InsertSort.InnerLoop",
        };
    }
}
