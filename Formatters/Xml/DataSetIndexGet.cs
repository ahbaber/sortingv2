﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Xml
{
	public class DataSetIndexGet : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet_index_get";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			string index = node.Parameters.First(p => p.Key == "index").Value;
			return builder.Append($"{indent}<DataSet_get i=\"{index}\">{node.Value}</DataSet_get>{newLine}");
		}

		public RuleData CreateDocumentation()
		{
			return RuleData.Construct("DataSet Index Parser", "Processes DataSet value reads", this)
				.AddData("DataSet", Tag);
		}
	}
}
