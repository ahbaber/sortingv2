﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using System.Text;
using Traceability;
using Traceability.Formatters;
using Traceability.Formatters.XmlSupport;

namespace Formatters.Xml
{
    public class HeapNodes : XmlNodeParser, INodeMultiTagParser, IDocumented
    {
        public List<string> Tags => _tags;

        public override StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
        {
            string i = node.Parameters["i"];
            builder.Append($"{indent}<{node.Tag} i=\"{i}\">{newLine}");
            ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
            return builder.Append($"{indent}</{node.Tag}>{newLine}");
        }

        public RuleData CreateDocumentation()
        {
            return RuleData.Construct("HeapSort Parser", "Processes multiple nodes for Heapsort", this)
                .AddData("HeapSort", "CheckChild MaxHeapUp SiftDown");
        }

        private static readonly List<string> _tags = new List<string>
        {
            "HeapSort.MaxHeapUp",
            "HeapSort.SiftDown",
            "HeapSort.CheckChild",
        };
    }
}
