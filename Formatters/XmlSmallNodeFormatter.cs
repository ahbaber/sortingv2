﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Xml;
using Traceability.Formatters;
using Traceability.Formatters.XmlSupport;

namespace Formatters
{
	public class XmlSmallNodeFormatter : NodeFormatter, IDocumented
	{
		public XmlSmallNodeFormatter()
		: base(new XmlNodeParser())
		{
			AddParser(_indexGet);
			AddParser(_swap);

			AddParser(_innerLoops);

			AddParser(_sorts);
			AddParser(_median);

			AddParser(_heapNodes);
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Xml Small Formatter",
				"Formats trace data as XML with parameters being represented as attribute key/values", this);
			docs.AddChild(_indexGet);
			docs.AddChild(_swap);
			docs.AddChild(_innerLoops);
			docs.AddChild(_sorts);
			docs.AddChild(_median);
			docs.AddChild(_heapNodes);
			return docs;
		}

		private readonly DataSetIndexGet _indexGet = new DataSetIndexGet();
		private readonly DataSetSwap _swap = new DataSetSwap();
		private readonly InnerLoops _innerLoops = new InnerLoops();
		private readonly SortsAndPartitions _sorts = new SortsAndPartitions();
		private readonly QuickSortGetMedianValue _median = new QuickSortGetMedianValue();
		private readonly HeapNodes _heapNodes = new HeapNodes();
	}
}
