﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.Quick;

namespace Formatters.Html.QuickInsert
{
	public class QuickInsertSortSort : QuickSortSort
	{
		public override string Tag => "QuickInsertSort.Sort";

		public QuickInsertSortSort(QuickInsertSortPartition partition = null)
		: base(partition ?? new QuickInsertSortPartition())
		{ }

		public override RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Insert Sort Inner", "Parses the inner quick sorts of quick sort", this)
				.AddData("Sort Type", "QuickInsertSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}
	}
}
