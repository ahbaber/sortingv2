﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.Quick;

namespace Formatters.Html.QuickInsert
{
	public class QuickInsertSortGetMedianValue : QuickSortGetMedianValue
	{
		public override string Tag => "QuickInsertSort.GetMedianValue";

		public override RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Insert Sort Median Value", "Parses the median value of the quick sort", this)
				.AddData("Sort Type", "QuickInsertSort")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
