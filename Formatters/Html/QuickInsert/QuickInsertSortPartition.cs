﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using Formatters.Html.Quick;

namespace Formatters.Html.QuickInsert
{
	public class QuickInsertSortPartition : QuickSortPartition
	{
		public override string Tag => "QuickInsertSort.Partition";

		public QuickInsertSortPartition(DataSetIndexGet indexGet = null, DataSetSwap swap = null, QuickInsertSortGetMedianValue median = null)
		: base(null, null, median ?? new QuickInsertSortGetMedianValue())
		{ }

		public override RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Insert Sort Partition", "Parses the quick sort partitioning", this)
				.AddData("Sort Type", "QuickInsertSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}
	}
}
