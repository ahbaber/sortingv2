﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.Insert;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.QuickInsert
{
	public class QuickInsertSortOuter : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "QuickInsertSort.Sort";

		public QuickInsertSortOuter(QuickInsertSortSort quick = null, InsertSortSort insert = null)
		{
			_quick = quick ?? new QuickInsertSortSort();
			_insert = insert ?? new InsertSortSort();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{

			using (var quick = map.AddTempParser(_quick))
			using (var insert = map.AddTempParser(_insert))
			{
				var quickNode = node.Children.Find(n => n.Tag == _quick.Tag);
				var insertNode = node.Children.Find(n => n.Tag == _insert.Tag);

				context.Set("parentId", 0)
					.Set("maxId", 0);
				builder.Append($"{newLine}{indent}<div id=\"QSWrapper\" style=\"display:inline-block\">");
				_quick.Build(builder, map, quickNode, context, indent + tab, tab, newLine);
				builder.Append($"{newLine}{indent}</div>");

				builder.Append($"{newLine}{indent}<div style=\"height:10px\"></div>");
				_insert.Build(builder, map, insertNode, context, indent + tab, tab, newLine);
			}
			context.Set("css", _css);
			context.Set("postscript", _postscript);

			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Insert Sort Outer", "Parses the initial quick insert sort call", this)
				.AddData("Sort Type", "QuickInsertSort")
				.AddData("Tag", Tag);
			docs.AddChild(_quick);
			docs.AddChild(_insert);
			return docs;
		}

		private readonly QuickInsertSortSort _quick;
		private readonly InsertSortSort _insert;
		private const string _css = @"
.GetValue{height:10px;padding-left:10px;background-color:yellow;font-size:8px;}
.InnerLoop{display:inline-block;position:relative;vertical-align:top;border-style:solid;border-color:black;border-width:2px;margin-bottom:10px;width:45px;margin-right:40px;}
.Median{height:10px;padding-left:10px;background-color:purple;font-size:8px;color:white;}
.Partition{position:absolute;top:25px;left:5px;width:45px;border-style:solid;border-color:black;border-width:1px;}
.PostCheck{display:inline-block;vertical-align:top;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;background-color:lightgreen;}
.PreCheck{display:inline-block;vertical-align:top;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;}
.PreValue{display:block;height:20px;font-size:12px;}
.QRange{position:absolute;top:3;left:10;display:block;}
.QSort{position:relative;display:inline-block;padding-top:25px;padding-bottom:5px;padding-left:60px;background-color:lightblue;border-style:solid;border-color:black;border-width:1px;width:92px;}
.Range{display:block;height:15px;font-size:10px;}
.ReadSwaps{display:block;position:absolute;left:40;width:40px;}
.ResponseValue{display:block;height:20px;font-size:12px;}
.Sorted{display:block;background-color:lightgreen;}
.SwapValue{height:5px;background-color:red;}
.Unsorted{background-color:lightgrey;}";
		private const string _postscript = @"
<script>
function UpdateQSortHeight(qSort)
{
    var partition = qSort.getElementsByClassName(""parParent"" + qSort.id);
    if (partition == null || partition.length == 0)
    {
        var size =
        {
            x: qSort.offsetWidth,
            y: qSort.offsetHeight,
        }
        return size;
    }
    var partitionHeight = partition[0].offsetHeight;

    var children = qSort.getElementsByClassName(""parent"" + qSort.id);
    var child0 = UpdateQSortHeight(children[0]);
    var child1 = UpdateQSortHeight(children[1]);
    var childHeight = child0.y + child1.y;

    var qSortHeight = Math.max(partitionHeight, childHeight) + 25;
    qSort.style.height = qSortHeight + ""px"";

    var width = Math.max(child0.x, child1.x) + 60;
    var size =
    {
        x: width,
        y: qSort.offsetHeight,
    }
    return size;
}
var size = UpdateQSortHeight(document.getElementById(""1""));
document.getElementById(""QSWrapper"").style.width = size.x + 10;
</script>
<script>
var elements = document.getElementsByClassName(""InnerLoop"")
var maxHeight = 0;
if (elements != null)
{
    for (var i = 0; i < elements.length; ++i)
    {
        var e = elements[i];
        var height = e.getElementsByClassName(""Sorted"")[0].offsetHeight;
        height += e.getElementsByClassName(""Range"")[0].offsetHeight;
        height += e.getElementsByClassName(""Unsorted"")[0].offsetHeight;
        maxHeight = Math.max(maxHeight, height);
    }
    for (var i = 0; i < elements.length; ++i)
    {
        var e = elements[i];
        var unsorted = e.getElementsByClassName(""Unsorted"")[0];
        var diffHeight = maxHeight - e.offsetHeight
        unsorted.style.height = unsorted.offsetHeight + diffHeight;
    }
}
if (maxHeight > 0)
{
    var pre = document.getElementsByClassName(""PreCheck"");
    if (pre != null && pre.length > 0)
    { pre[0].style.height = maxHeight-12; }
    var post = document.getElementsByClassName(""PostCheck"");
    if (post != null && post.length > 0)
    { post[0].style.height = maxHeight-12;}
}
</script>";
	}
}
