﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.DataSet
{
	public class DataSetResponseData : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet.ResponseData";

		public DataSetResponseData(PostDataSetIndexGet indexGet = null)
		{
			_indexGet = indexGet ?? new PostDataSetIndexGet();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			builder.Append($"{newLine}{indent}<div class=\"PostCheck\">");
			using (var indexGet = map.AddTempParser(_indexGet))
			{
				ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
			}
			return builder.Append($"{newLine}{indent}</div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("PostCheckData", "Examines the DataSet after sorting", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			docs.AddChild(_indexGet);
			return docs;
		}

		private readonly PostDataSetIndexGet _indexGet;
	}
}
