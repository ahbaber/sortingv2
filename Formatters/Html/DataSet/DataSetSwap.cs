﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.DataSet
{
	public class DataSetSwap : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet.Swap";

		public virtual StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			return builder.AppendFormat($"{newLine}{indent}<div class=\"SwapValue\"></div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Swap", "Parses each swap in the DataSet", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
