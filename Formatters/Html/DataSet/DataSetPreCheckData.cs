﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.DataSet
{
	public class DataSetPreCheckData : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet.PreCheckData";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			builder.Append($"\n{indent}<div class=\"PreCheck\">");
			using (var indexGet = map.AddTempParser(_indexGet))
			{
				ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
			}
			return builder.Append($"\n{indent}</div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("PreCheckData", "Examines the DataSet prior to sorting", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			docs.AddChild(_indexGet);
			return docs;
		}

		private readonly PreDataSetIndexGet _indexGet = new PreDataSetIndexGet();
	}
}
