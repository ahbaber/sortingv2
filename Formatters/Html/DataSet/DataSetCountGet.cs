﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.DataSet
{
	public class DataSetCountGet : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet.Count_get";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			context.Set(node.Tag, Convert.ToInt32(node.Value));
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Count Get", "Parses the size of the DataSet", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
