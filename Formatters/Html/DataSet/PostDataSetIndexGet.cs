﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.DataSet
{
	public class PostDataSetIndexGet : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet_index_get";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			string index = node.Parameters.First(p => p.Key == "index").Value;
			return builder.Append($"{newLine}{indent}<div class=\"ResponseValue\">{index}: {node.Value}</div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Post Index Get", "Parses each read from the DataSet after sorting", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
