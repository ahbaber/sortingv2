﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Simple
{
	public class SimpleSortSort : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public SimpleSortSort(DataSetCountGet countGet = null, SimpleSortInnerLoop innerLoop = null)
		{
			_countGet = countGet ?? new DataSetCountGet();
			_innerLoop = innerLoop ?? new SimpleSortInnerLoop();
		}

		public string Tag => "SimpleSort.Sort";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			context.Set("css", _css);
			context.Set("postscript", _postscript);
			using (var countGet = map.AddTempParser(_countGet))
			using (var innerLoop = map.AddTempParser(_innerLoop))
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
			}
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Simple Sort", "Parses a simple sort", this)
				.AddData("Sort Type", "SimpleSort")
				.AddData("Tag", Tag);
			docs.AddChild(_countGet);
			docs.AddChild(_innerLoop);
			return docs;
		}

		private readonly DataSetCountGet _countGet;
		private readonly SimpleSortInnerLoop _innerLoop;

		private const string _css = @"
.CompareGroup{border-style:solid;border-color:darkgrey;border-width:1px;}
.GetValue{height:10px;padding-left:10px;background-color:yellow;font-size:8px;}
.InnerLoop{display:inline-block;position:relative;vertical-align:top;border-style: solid;border-color: black;border-width:2px;margin-bottom:10px;width:45px;}
.PostCheck{display:inline-block;border-style:solid;border-color:black;border-width:1px;width:38px;padding-top:10px;padding-left:10px;background-color:lightgreen;}
.PreCheck{display:inline-block;border-style:solid;border-color:black;border-width:1px;width:38px;padding-top:10px;padding-left:10px;}
.PreValue{display:block;height:20px;font-size:12px;}
.Range{display:block;height:10px;font-size:10px;}
.ReadSwaps{display:block;position:absolute;bottom:0;width:45px;background-color:blue;}
.ResponseValue{display:block;height:20px;font-size:12px;}
.Sorted{display: block;background-color:lightgreen;}
.SwapValue{height:5px;background-color:red;}";
		private const string _postscript = @"
<script>
	var elements = document.getElementsByClassName(""InnerLoop"")
	var maxHeight = 0;
	for (var i = 0; i < elements.length; ++i)
	{
		var e = elements[i];
		var height = e.getElementsByClassName(""Sorted"")[0].offsetHeight;
		height += e.getElementsByClassName(""Range"")[0].offsetHeight;
		height += e.getElementsByClassName(""ReadSwaps"")[0].offsetHeight;
		maxHeight = Math.max(maxHeight, height);
	}
	var preHeight = maxHeight - 8;
	for (var i = 0; i < elements.length; ++i)
	{ elements[i].style.height = maxHeight; }
	var pre = document.getElementsByClassName(""PreCheck"");
	if (pre != null && pre.length > 0)
	{ pre[0].style.height = preHeight; }
	var post = document.getElementsByClassName(""PostCheck"");
	if (post != null && post.length > 0)
	{ post[0].style.height = preHeight;}
</script>";
	}
}
