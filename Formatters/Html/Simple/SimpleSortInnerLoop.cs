﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Simple
{
	public class SimpleSortInnerLoop : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public SimpleSortInnerLoop(DataSetIndexGet indexGet = null, DataSetSwap swap = null)
		{
			_indexGet = indexGet ?? new DataSetIndexGet();
			_swap = swap ?? new DataSetSwap();
		}

		public virtual string Tag => "SimpleSort.InnerLoop";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			int parameterI = Convert.ToInt32(node.Parameters.First(p => p.Key.Equals("i")).Value);
			int dataSize = context.GetInt("DataSet.Count_get") - 1;

			builder.Append($"{newLine}{indent}<div class=\"InnerLoop\">")
				.AppendFormat($"{newLine}{indent}{tab}<div class=\"Sorted\" style=\"height:{parameterI * 20}px\"></div>")
				.Append($"{newLine}{indent}{tab}<div class=\"Range\">{parameterI}-{dataSize}</div>")
				.Append($"{newLine}{indent}{tab}<div class=\"ReadSwaps\">");

			string readGroupIndent = indent + tab + tab;
			string childIndent = readGroupIndent + tab;
			int i = 0;
			while (i < node.Children.Count - 1) // last node is DataSet.Count_get
			{
				// each CompareGroup consists of 3 or 4 nodes, with the 4th node being variable
				// node 0: DataSet.Count_get is an artifact of the loop, and its data is not included in the group
				// node 1: DataSet.Index_get is the first element being read for comparison
				// node 2: Dataset.Index_get is the second element being read for comparison
				// node 3: DataSet.Swap only exists if the two elements were swapped.

				builder.Append($"{newLine}{readGroupIndent}<div class=\"CompareGroup\">");
				// +0 is DataSet.Count_get which is being ignored here
				_indexGet.Build(builder, map, node.Children[i + 1], context, childIndent, tab, newLine);
				_indexGet.Build(builder, map, node.Children[i + 2], context, childIndent, tab, newLine);
				if (HasSwap(node.Children, i + 3))
				{
					_swap.Build(builder, map, node.Children[i + 3], context, childIndent, tab, newLine);
					++i; // accounts for the swap
				}
				builder.Append($"{newLine}{readGroupIndent}</div>");

				i += 3;
			}

			return builder.Append($"{newLine}{indent}{tab}</div>{newLine}{indent}</div>");
		}

		public virtual RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Simple Inner Loop", "Parses the inner loop of a simple sort", this)
				.AddData("Sort Type", "SimpleSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}

		protected void AddChildDocs(RuleData docs)
		{
			docs.AddChild(_indexGet);
			docs.AddChild(_swap);
		}

		private readonly DataSetIndexGet _indexGet;
		private readonly DataSetSwap _swap;

		private bool HasSwap(List<TraceNode> children, int swapIndex)
		{
			if (swapIndex > children.Count)
			{ return false; }
			return children[swapIndex].Tag.Equals(_swap.Tag);
		}
	}
}
