﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using Formatters.Html.Insert;
using Formatters.Html.Quick;
using Formatters.Html.QuickInsert;
using Formatters.Html.Heap;
using Formatters.Html.Selection;
using Formatters.Html.Simple;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html
{
	public class Root : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "Root";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			using (var pre = map.AddTempParser(_pre))
			using (var response = map.AddTempParser(_reponse))
			using (var simple = map.AddTempParser(_simple))
			using (var selection = map.AddTempParser(_selection))
			using (var insert = map.AddTempParser(_insert))
			using (var quick = map.AddTempParser(_quick))
			using (var quickInsert = map.AddTempParser(_quickInsert))
			using (var heap = map.AddTempParser(_heap))
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
			}

			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Root", "Parses the root node", this)
				.AddData("Sort Type", "N/A")
				.AddData("Tag", Tag);
			docs.AddChild(_pre);
			docs.AddChild(_simple);
			docs.AddChild(_selection);
			docs.AddChild(_insert);
			docs.AddChild(_quick);
			docs.AddChild(_quickInsert);
			docs.AddChild(_heap);
			docs.AddChild(_reponse);
			return docs;
		}

		private readonly DataSetPreCheckData _pre = new DataSetPreCheckData();
		private readonly DataSetResponseData _reponse = new DataSetResponseData();
		private readonly SimpleSortSort _simple = new SimpleSortSort();
		private readonly SelectionSortSort _selection = new SelectionSortSort();
		private readonly InsertSortSort _insert = new InsertSortSort();
		private readonly QuickSortSortOuter _quick = new QuickSortSortOuter();
		private readonly QuickInsertSortOuter _quickInsert = new QuickInsertSortOuter();
		private readonly HeapSortSort _heap = new HeapSortSort();
	}
}
