﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Quick
{
	public class QuickSortGetMedianValue : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public virtual string Tag => "QuickSort.GetMedianValue";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			return builder.Append($"{newLine}{indent}<div class=\"Median\">{node.Value}</div>");
		}

		public virtual RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Sort Median Value", "Parses the median value of the quick sort", this)
				.AddData("Sort Type", "QuickSort")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
