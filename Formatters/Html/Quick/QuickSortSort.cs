﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Quick
{
	public class QuickSortSort : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public virtual string Tag => "QuickSort.Sort";

		public QuickSortSort(QuickSortPartition partition = null)
		{
			_partition = partition ?? new QuickSortPartition();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			int maxId = context.GetInt("maxId");
			int id = ++maxId;
			var childContext = new Context()
				.Set("parentId", id)
				.Set("maxId", id);

			UpdateMaxId(context, childContext);

			var childBuilder = new StringBuilder();
			string childIndent = indent + "\t";
			using (var partition = map.AddTempParser(_partition))
			{
				ProcessChildren(childBuilder, map, node, childContext, childIndent, tab, newLine);
			}

			string style = childBuilder.Length == 0
				? " style=\"width:45px;height:15px;\""
				: "";

			int parentId = context.GetInt("parentId");
			int start = Convert.ToInt32(node.Parameters.First(p => p.Key == "start").Value);
			int end = Convert.ToInt32(node.Parameters.First(p => p.Key == "end").Value);

			builder.Append($"{newLine}{indent}<div id=\"{id}\" class=\"QSort parent{parentId}\"{style}>")
				.Append($"{newLine}{childIndent}<div class=\"QRange\">{start}-{end}</div>")
				.Append(childBuilder)
				.AppendFormat($"{newLine}{indent}</div>");

			UpdateMaxId(context, childContext);

			return builder;
		}

		public virtual RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Sort Inner", "Parses the inner quick sorts of quick sort", this)
				.AddData("Sort Type", "QuickSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}

		protected void AddChildDocs(RuleData docs)
		{
			docs.AddChild(_partition);
		}

		private void UpdateMaxId(Context context, Context childContext)
		{
			int maxId = Math.Max(context.GetInt("maxId"), childContext.GetInt("maxId"));
			context.Set("maxId", maxId);
		}

		private readonly QuickSortPartition _partition;
	}
}
