﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Quick
{
	public class QuickSortSortOuter : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "QuickSort.Sort";

		public QuickSortSortOuter(QuickSortSort quickSort = null)
		{
			_quickSort = quickSort ?? new QuickSortSort();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			context.Set("css", _css)
				.Set("postscript", _postscript)
				.Set("parentId", 0)
				.Set("maxId", 0);
			builder.Append($"{newLine}{indent}<div id=\"QSWrapper\" style=\"display:inline-block\">");
			using (var quickSort = map.AddTempParser(_quickSort))
			{
				ProcessChildren(builder, map, node, context, indent + tab, tab, newLine);
			}
			return builder.Append($"{newLine}{indent}</div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Sort Outer", "Parses the initial quick sort call", this)
				.AddData("Sort Type", "QuickSort")
				.AddData("Tag", Tag);
			docs.AddChild(_quickSort);
			return docs;
		}

		private readonly QuickSortSort _quickSort;
		private const string _css = @"
.GetValue{height:10px;padding-left:10px;background-color:yellow;font-size:8px;}
.Median{height:10px;padding-left:10px;background-color:purple;font-size:8px;color:white;}
.Partition{position:absolute;top:25px;left:5px;width:45px;border-style:solid;border-color:black;border-width:1px;}
.PostCheck{display:inline-block;vertical-align:top;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;background-color:lightgreen;}
.PreCheck{display:inline-block;vertical-align:top;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;}
.PreValue{display:block;height:20px;font-size:12px;}
.QRange{position:absolute;top:3;left:10;display:block;}
.QSort{position:relative;display:inline-block;padding-top:25px;padding-bottom:5px;padding-left:60px;background-color:lightblue;border-style:solid;border-color:black;border-width:1px;width:92px;}
.ResponseValue{display:block;height:20px;font-size:12px;}
.SwapValue{height:5px;background-color:red;}";
		private const string _postscript = @"
<script>
function UpdateQSortHeight(qSort)
{
    var partition = qSort.getElementsByClassName(""parParent"" + qSort.id);
    if (partition == null || partition.length == 0)
    {
        var size =
        {
            x: qSort.offsetWidth,
            y: qSort.offsetHeight,
        }
        return size;
    }
    var partitionHeight = partition[0].offsetHeight;

    var children = qSort.getElementsByClassName(""parent"" + qSort.id);
    var child0 = UpdateQSortHeight(children[0]);
    var child1 = UpdateQSortHeight(children[1]);
    var childHeight = child0.y + child1.y;

    var qSortHeight = Math.max(partitionHeight, childHeight) + 25;
    qSort.style.height = qSortHeight + ""px"";

    var width = Math.max(child0.x, child1.x) + 60;
    var size =
    {
        x: width,
        y: qSort.offsetHeight,
    }
    return size;
}
var size = UpdateQSortHeight(document.getElementById(""1""));
document.getElementById(""QSWrapper"").style.width = size.x + 10;
</script>";
	}
}
