﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Quick
{
	public class QuickSortPartition : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public virtual string Tag => "QuickSort.Partition";

		public QuickSortPartition(DataSetIndexGet indexGet = null, DataSetSwap swap = null, QuickSortGetMedianValue median = null)
		{
			_indexGet = indexGet ?? new DataSetIndexGet();
			_swap = swap ?? new DataSetSwap();
			_median = median ?? new QuickSortGetMedianValue();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			var childBuilder = new StringBuilder();
			using (var median = map.AddTempParser(_median))
			using (var indexGet = map.AddTempParser(_indexGet))
			using (var swap = map.AddTempParser(_swap))
			{
				ProcessChildren(childBuilder, map, node, context, indent + tab, tab, newLine);
			}
			int parentId = context.GetInt("parentId");
			builder.Append($"{newLine}{indent}<div class=\"Partition parParent{parentId}\">")
				.Append(childBuilder)
				.Append($"{newLine}{indent}</div>");
			return builder;
		}

		public virtual RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Quick Sort Partition", "Parses the quick sort partitioning", this)
				.AddData("Sort Type", "QuickSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}

		protected void AddChildDocs(RuleData docs)
		{
			docs.AddChild(_median);
			docs.AddChild(_indexGet);
			docs.AddChild(_swap);
		}

		private readonly QuickSortGetMedianValue _median;
		private readonly DataSetIndexGet _indexGet;
		private readonly DataSetSwap _swap;
	}
}
