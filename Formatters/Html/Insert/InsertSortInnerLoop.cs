﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Insert
{
	public class InsertSortInnerLoop : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "InsertSort.InnerLoop";

		public InsertSortInnerLoop(DataSetIndexGet indexGet = null)
		{
			_indexGet = indexGet ?? new DataSetIndexGet();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			context.Set("shifts", 0);
			var childBuilder = new StringBuilder();

			using (var indexGet = map.AddTempParser(_indexGet))
			using (var innerSwap = map.AddTempParser(_innerSwap))
			{
				ProcessChildren(childBuilder, map, node, context, indent + tab, tab, newLine);
			}

			int i = Convert.ToInt32(node.Parameters.First(p => p.Key == "i").Value);
			int shifts = context.GetInt("shifts");
			int final = i - shifts;
			int setSize = context.GetInt("DataSet.Count_get");

			builder.Append($"{newLine}{indent}<div class=\"InnerLoop\">")
				.Append($"{newLine}{indent}{tab}<div class=\"Sorted\" style=\"height:{i * 25}px\"></div>")
				.Append($"{newLine}{indent}{tab}<div class=\"Range\">{i}->{final}</div>")
				.Append($"{newLine}{indent}{tab}<div class=\"ReadSwaps\" style=\"top:{final * 25}px\">")
				.Append(childBuilder)
				.Append($"{newLine}{indent}{tab}</div>")
				.Append($"{newLine}{indent}{tab}<div class=\"Unsorted\" style=\"height:{(setSize - i) * 15}px\"></div>")
				.Append($"{newLine}{indent}</div>");
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Insert Inner Loop", "Parses the inner loop of an insert sort", this)
				.AddData("Sort Type", "InsertSort")
				.AddData("Tag", Tag);
			docs.AddChild(_indexGet);
			return docs;
		}

		private readonly DataSetIndexGet _indexGet;
		private readonly InnerSwap _innerSwap = new InnerSwap();

		private class InnerSwap : DataSetSwap
		{
			public override StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				context.Set("shifts", context.GetInt("shifts") + 1);
				return base.Build(builder, map, node, context, indent, tab, newLine);
			}
		}
	}
}
