﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Insert
{
	public class InsertSortSort : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "InsertSort.Sort";

		public InsertSortSort(DataSetCountGet countGet = null, InsertSortInnerLoop innerLoop = null)
		{
			_countGet = countGet ?? new DataSetCountGet();
			_innerLoop = innerLoop ?? new InsertSortInnerLoop();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			context.Set("css", _css);
			context.Set("postscript", _postscript);
			using (var countGet = map.AddTempParser(_countGet))
			using (var innerLoop = map.AddTempParser(_innerLoop))
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
			}
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Insert Sort", "Parses an insert sort", this)
				.AddData("Sort Type", "InsertSort")
				.AddData("Tag", Tag);
			docs.AddChild(_countGet);
			docs.AddChild(_innerLoop);
			return docs;
		}

		private readonly DataSetCountGet _countGet;
		private readonly InsertSortInnerLoop _innerLoop;
		private const string _css = @"
.GetValue{height:10px;padding-left:10px;background-color:yellow;font-size:8px;}
.InnerLoop{display:inline-block;position:relative;vertical-align:top;border-style:solid;border-color:black;border-width:2px;margin-bottom:10px;width:45px;margin-right:40px;}
.PostCheck{display:inline-block;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;background-color:lightgreen;}
.PreCheck{display:inline-block;border-style:solid;border-color:black;border-width:1px;width:40px;padding-top:10px;padding-left:10px;margin-right:37px;}
.PreValue{display:block;height:20px;font-size:12px;}
.Range{display:block;height:15px;font-size:10px;}
.ReadSwaps{display:block;position:absolute;left:40;width:40px;}
.ResponseValue{display:block;height:20px;font-size:12px;}
.Sorted{display:block;background-color:lightgreen;}
.SwapValue{height:5px;background-color:red;}
.Unsorted{background-color:lightgrey;}";
		private const string _postscript = @"
<script>
var elements = document.getElementsByClassName(""InnerLoop"")
var maxHeight = 0;
if (elements != null)
{
    for (var i = 0; i < elements.length; ++i)
    {
        var e = elements[i];
        var height = e.getElementsByClassName(""Sorted"")[0].offsetHeight;
        height += e.getElementsByClassName(""Range"")[0].offsetHeight;
        height += e.getElementsByClassName(""Unsorted"")[0].offsetHeight;
        maxHeight = Math.max(maxHeight, height);
    }
    for (var i = 0; i < elements.length; ++i)
    {
        var e = elements[i];
        var unsorted = e.getElementsByClassName(""Unsorted"")[0];
        var diffHeight = maxHeight - e.offsetHeight
        unsorted.style.height = unsorted.offsetHeight + diffHeight;
    }
}
if (maxHeight > 0)
{
    var pre = document.getElementsByClassName(""PreCheck"");
    if (pre != null && pre.length > 0)
    { pre[0].style.height = maxHeight-12; }
    var post = document.getElementsByClassName(""PostCheck"");
    if (post != null && post.length > 0)
    { post[0].style.height = maxHeight-12;}
}
</script>";
	}
}
