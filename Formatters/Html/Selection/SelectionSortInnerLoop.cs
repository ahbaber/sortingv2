﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using Formatters.Html.Simple;

namespace Formatters.Html.Selection
{
	public class SelectionSortInnerLoop : SimpleSortInnerLoop, IDocumented
	{
		public SelectionSortInnerLoop(DataSetIndexGet indexGet = null, DataSetSwap swap = null)
		: base(indexGet, swap)
		{ }

		public override string Tag => "SelectionSort.InnerLoop";

		public override RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Selection Inner Loop", "Parses the inner loop of a selection sort", this)
				.AddData("Sort Type", "SelectionSort")
				.AddData("Tag", Tag);
			AddChildDocs(docs);
			return docs;
		}
	}
}
