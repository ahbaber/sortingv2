﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Heap
{
	public class HeapSortSiftDown : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "HeapSort.SiftDown";

		public HeapSortSiftDown(HeapSortDataSetIndexGet indexGet = null, HeapSortDataSetSwap swap = null)
		{
			_indexGet = indexGet ?? new HeapSortDataSetIndexGet();
			_swap = swap ?? new HeapSortDataSetSwap();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			var childContext = new Context();
			var childBuilder = new StringBuilder();

			using (var indexGet = map.AddTempParser(_indexGet))
			using (var swap = map.AddTempParser(_swap))
			using (var checkChild = map.AddTempParser(_checkChild))
			using (var siftDown = map.AddTempParser("HeapSort.SiftDown", _checkChild))
			{
				ProcessChildren(childBuilder, map, node, childContext, indent + tab, tab, newLine);
			}

			int dataSize = context.GetInt("DataSet.Count_get");
			int height = (dataSize * 15) - 5;
			int last = Convert.ToInt32(node.Parameters.First(p => p.Key == "last").Value);
			int sortedHeight = last == 0 ? height : ((dataSize - last) * 15) - 20;
			builder.Append($"{newLine}{indent}<div class=\"SiftDown\" style=\"height:{height}\">")
				.Append(childBuilder)
				.Append($"{newLine}{indent}{tab}<div class=\"Sorted\" style=\"height:{sortedHeight}\"></div>")
				.Append($"{newLine}{indent}</div>");

			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Heap Sift Down", "Parses heap sort sift down", this)
				.AddData("Sort Type", "HeapSort")
				.AddData("Tag", Tag);
			docs.AddChild(_indexGet);
			docs.AddChild(_swap);
			docs.AddChild(_checkChild);
			return docs;
		}

		private readonly HeapSortDataSetIndexGet _indexGet;
		private readonly HeapSortDataSetSwap _swap;
		private readonly CheckChild _checkChild = new CheckChild();

		private class CheckChild : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
		{
			public string Tag => "HeapSort.CheckChild";

			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
				return builder;
			}

			public RuleData CreateDocumentation()
			{
				var docs = RuleData.Construct("Heap Check Child", "Parses heap sort check child", this)
					.AddData("Sort Type", "HeapSort")
					.AddData("Tag", Tag);
				return docs;
			}
		}
	}
}
