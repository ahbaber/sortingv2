﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Heap
{
	public class HeapSortMaxHeapUp : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "HeapSort.MaxHeapUp";

		public HeapSortMaxHeapUp(HeapSortDataSetIndexGet indexGet = null, HeapSortDataSetSwap swap = null)
		{
			_indexGet = indexGet ?? new HeapSortDataSetIndexGet();
			_swap = swap ?? new HeapSortDataSetSwap();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			var childContext = new Context();
			var childBuilder = new StringBuilder();

			using (var indexGet = map.AddTempParser(_indexGet))
			using (var swap = map.AddTempParser(_swap))
			using (var childMaxHeapUp = map.AddTempParser(_childMaxHeapUp))
			{
				ProcessChildren(childBuilder, map, node, childContext, indent + tab, tab, newLine);
			}

			int dataSize = (context.GetInt("DataSet.Count_get") * 15) - 5;
			int unsortedTop = (childContext.GetInt("maxIndex") + 1) * 15;
			builder.Append($"{newLine}{indent}<div class=\"MaxHeapUp\" style=\"height:{dataSize}\">")
				.Append(childBuilder)
				.Append($"{newLine}{indent}{indent}<div class=\"Unsorted\" style=\"top:{unsortedTop}\"></div>")
				.Append($"{newLine}{indent}</div>");

			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Heap Max Heap Up", "Parses heap sort max heap up", this)
				.AddData("Sort Type", "HeapSort")
				.AddData("Tag", Tag);
			docs.AddChild(_indexGet);
			docs.AddChild(_swap);
			return docs;
		}

		private readonly HeapSortDataSetIndexGet _indexGet;
		private readonly HeapSortDataSetSwap _swap;
		private readonly ChildMaxHeapup _childMaxHeapUp = new ChildMaxHeapup();

		private class ChildMaxHeapup : AbstractNodeChildProcessor, INodeTaggedParser
		{
			public string Tag => "HeapSort.MaxHeapUp";

			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
				return builder;
			}
		}
	}
}
