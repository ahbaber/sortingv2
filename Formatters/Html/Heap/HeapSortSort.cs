﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Formatters.Html.DataSet;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Heap
{
	public class HeapSortSort : AbstractNodeChildProcessor, INodeTaggedParser, IDocumented
	{
		public string Tag => "HeapSort.Sort";

		public HeapSortSort(DataSetCountGet countGet = null, HeapSortMaxHeapUp maxHeapUp = null, HeapSortSiftDown siftDown = null)
		{
			_countGet = countGet ?? new DataSetCountGet();
			_maxHeapUp = maxHeapUp ?? new HeapSortMaxHeapUp();
			_siftDown = siftDown ?? new HeapSortSiftDown();
		}

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			using (var countGet = map.AddTempParser(_countGet))
			using (var maxHeapUp = map.AddTempParser(_maxHeapUp))
			using (var siftDown = map.AddTempParser(_siftDown))
			{
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
			}

			context.Set("css", _css);
			context.Set("postscript", _postscript);

			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Heap Sort", "Parses heap sort", this)
				.AddData("Sort Type", "HeapSort")
				.AddData("Tag", Tag);
			docs.AddChild(_countGet);
			docs.AddChild(_maxHeapUp);
			docs.AddChild(_siftDown);
			return docs;
		}

		private readonly DataSetCountGet _countGet;
		private readonly HeapSortMaxHeapUp _maxHeapUp;
		private readonly HeapSortSiftDown _siftDown;

		private const string _css = @"
.PreCheck{display:inline-block;vertical-align:top;margin-right:5px;border-style:solid;border-color:black;border-width:1px;width:30px;padding-top:10px;padding-left:10px;}
.PostCheck{display:inline-block;vertical-align:top;border-style:solid;border-color:black;border-width:1px;width:30px;padding-top:10px;padding-left:10px;background-color:lightgreen;}
.ResponseValue{display:block;height:10px;font-size:10px;}
.PreValue{display:block;height:10px;font-size:10px;}
.HeapGetValue{position:absolute;left:0;width:28px;border-style:solid;border-color:black;border-width:1px;height:10px;padding-left:10px;background-color:yellow;font-size:8px;}
.HeapSwapValue{position:absolute;left:5;width:5px;background-color:red;}
.MaxHeapUp{position:relative;display:inline-block;margin-bottom:10px;margin-right:5px;background-color:blue;border-style:solid;border-color:black;border-width:1px;width:40px;}
.Unsorted{position:absolute;bottom:0;display:block;background-color:grey;width:40px;}
.SiftDown{position:relative;display:inline-block;margin-bottom:10px;margin-right:5px;background-color:blue;border-style:solid;border-color:black;border-width:1px;width:40px;}
.Sorted{position:absolute;bottom:0;display:block;background-color:lightgreen;width:40px;}";
		private const string _postscript = @"
<script>
var elements = document.getElementsByClassName(""MaxHeapUp"");
if (elements != null && elements.length > 0)
{
    var height = elements[0].offsetHeight - 12;
    var pre = document.getElementsByClassName(""PreCheck"");
    if (pre != null && pre.length > 0)
    {
        pre[0].style.height = height;
    }
    var post = document.getElementsByClassName(""PostCheck"");
    if (post != null && post.length > 0)
    {
        post[0].style.height = height;
    }
}
</script>";
	}
}
