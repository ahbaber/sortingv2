﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Heap
{
	public class HeapSortDataSetSwap : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet.Swap";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (!node.HasParameters || node.Parameters.Count < 2)
			{ return builder; }

			int start = Convert.ToInt32(node.Parameters.First(p => p.Key == "i").Value);
			int end = Convert.ToInt32(node.Parameters.First(p => p.Key == "j").Value);

			if (start > end)
			{ (end, start) = (start, end); }
			start = start * 15 + 12;
			end = end * 15 - start;

			return builder.Append($"{newLine}{indent}<div class=\"HeapSwapValue\" style=\"top:{start};height:{end}\"></div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Heap Swap", "Parses a swap inside of heap sort", this)
				.AddData("Sort Type", "HeapSort")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
