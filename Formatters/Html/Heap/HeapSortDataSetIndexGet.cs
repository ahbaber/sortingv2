﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using System.Linq;
using System.Text;
using Traceability;
using Traceability.Formatters;

namespace Formatters.Html.Heap
{
	public class HeapSortDataSetIndexGet : INodeTaggedParser, IDocumented
	{
		public string Tag => "DataSet_index_get";

		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (!node.HasParameters)
			{ return builder; }

			// we only want to display an index once, but the recursive nature of MaxHeapUp causes the same index
			// to be examined multiple times.  So we use the context as a map, and ignore subsequent examinations
			string index = node.Parameters.First(p => p.Key == "index").Value;
			if (context.GetBool(index))
			{ return builder; }
			context.Set(index, true);

			int i = Convert.ToInt32(index);
			context.Set("maxIndex", Math.Max(i, context.GetInt("maxIndex")));
			i *= 15;
			return builder.AppendFormat($"{newLine}{indent}<div class=\"HeapGetValue\" style=\"top:{i}\">{index}: {node.Value}</div>");
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Heap Index Get", "Parses each read from the DataSet", this)
				.AddData("Sort Type", "HeapSort")
				.AddData("Tag", Tag);
			return docs;
		}
	}
}
