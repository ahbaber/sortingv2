﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;
using Formatters.Html;
using Traceability;
using Traceability.Formatters;

namespace Formatters
{
	public class HtmlNodeFormatter : NodeFormatter, IDocumented
	{
		public HtmlNodeFormatter()
		: base(new EmptyNodeParser())
		{
			AddParser(_root);
		}

		public override StringBuilder Build(StringBuilder builder, TraceNode node)
		{
			SetIndent("\t");
			var childBuilder = new StringBuilder();
			base.Build(childBuilder, node);
			string css = _context.GetString("css");
			string postscript = _context.GetString("postscript");

			return builder.AppendFormat(@"<html>
<head>
<style>{0}
</style>
</head>
<body>{1}
</body>
</html>{2}", css, childBuilder, postscript);
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("HTML Node Formatter", "Extracts trace data and formats it as HTML", this);
			docs.AddChild(_root);
			return docs;
		}

		private readonly Root _root = new Root();
	}
}
