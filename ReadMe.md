#TraceSorting V2

ReadMe info is now in the [wiki](https://bitbucket.org/ahbaber/sortingv2/wiki/Home).


Change Log

2019 Feb 26 v2.3.1

 * Reorganized code into sub projectes to better illustrate various parts.
 * Added intermediate trace level to demonstrate more fine grained control.
 * ReadMe info moved to Wiki pages.

2019 Feb 19 v2.3.0

 * Moved Sorting routines over to [Traceability Tutorial](https://bitbucket.org/ahbaber/traceabilitytutorial/)

2019 Feb 13

 * Updated to match with TraceabilityV2

2019 Feb 11

 * Updated to match with TraceabilityV2

2019 Feb 09

 * Updated to match with TraceabilityV2

2019 Jan 29

 * Documented Sorting routines.

2019 Jan 28

 * Initial checkin
