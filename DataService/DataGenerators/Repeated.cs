﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using System.Linq;

namespace DataService.DataGenerators
{
	public class Repeated : IDataGenerator
	{
		public string Name => "Repeated";

		public List<int> Generate(int size)
		{
			return Enumerable.Repeat(42, size).ToList<int>();
		}
	}
}
