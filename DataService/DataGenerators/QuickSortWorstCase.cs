﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using System.Linq;

namespace DataService.DataGenerators
{
	public class QuickSortWorstCase : IDataGenerator
	{
		public string Name => "Quick Sort Worst Case";

		public List<int> Generate(int size)
		{
            int initial = size % 2 == 0
                ? 1
                : 2;
            var data = Enumerable.Repeat(initial, size).ToList<int>();
            int value = data.Count - 1;
            int middle = (data.Count - 1) / 2;
            data[0] = value;

            for (int i = middle; i > 0; --i, value-=2)
            { data[i] = value; }

            value = data.Count - 3;
            int start = data.Count - 1;
            int step = 2;
            int offset = 1;

            while (value > 0)
            {
                for (int i = start; i > middle; i-=step, value-=2)
                { data[i] = value; }
                start -= offset;
                offset *= 2;
                step *= 2;
            }

			return data;
		}
	}
}
