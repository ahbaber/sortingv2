﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using System.Linq;

namespace DataService.DataGenerators
{
	public class Reverse : IDataGenerator
	{
		public string Name => "Reverse";

		public List<int> Generate(int size)
		{
			var data = Enumerable.Range(1, size).ToList<int>();
            data.Reverse();
			return data;
		}
	}
}
