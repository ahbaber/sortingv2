﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using System.Linq;

namespace DataService.DataGenerators
{
	public class Ordered : IDataGenerator
	{
		public string Name => "Ordered";

		public List<int> Generate(int size)
		{
			return Enumerable.Range(1, size).ToList<int>();
		}
	}
}
