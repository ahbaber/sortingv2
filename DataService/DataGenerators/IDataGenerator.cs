﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;

namespace DataService.DataGenerators
{
	public interface IDataGenerator
	{
		string Name { get; }
		List<int> Generate(int size);
	}
}
