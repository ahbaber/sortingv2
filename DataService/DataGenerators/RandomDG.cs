﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataService.DataGenerators
{
	public class RandomDG : IDataGenerator
	{
		public string Name => "Random";

		public List<int> Generate(int size)
		{
			int max = size * 2;
            return Enumerable.Range(0, size).Select(n => _random.Next(max)).ToList<int>();
		}

		private Random _random = new Random();
	}
}
