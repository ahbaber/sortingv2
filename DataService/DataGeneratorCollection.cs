﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using DataService.DataGenerators;
using System.Collections.Generic;

namespace DataService
{
	public class DataGeneratorCollection : IDataGeneratorCollection
	{
		public int Setting { get; private set; } = 0;

		public IDataGeneratorCollection AddDataGenerator(IDataGenerator dataGenerator)
		{
			_dataGenerators.Add(dataGenerator);
			return this;
		}

		public bool UpdateSetting(int command)
		{
			if (0 <= command && command < _dataGenerators.Count)
			{
				Setting = command;
				return true;
			}
			return false;
		}

		public List<int> GetData(int size)
		{
			return _dataGenerators[Setting].Generate(size);
		}

		private readonly List<IDataGenerator> _dataGenerators = new List<IDataGenerator>();
	}
}
