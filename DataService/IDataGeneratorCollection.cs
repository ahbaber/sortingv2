﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using DataService.DataGenerators;
using System.Collections.Generic;

namespace DataService
{
	public interface IDataGeneratorCollection
	{
		int Setting { get; }
		bool UpdateSetting(int newSetting);
		List<int> GetData(int size);
		IDataGeneratorCollection AddDataGenerator(IDataGenerator dataGenerator);
	}
}
