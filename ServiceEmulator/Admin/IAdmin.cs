﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;

namespace ServiceEmulator.Admin
{
	public interface IAdmin
	{
		string Title { get; }
		string SettingDescription { get; }
		int Setting { get; }
		bool UpdateSetting(int newSetting);
		IReadOnlyList<string> Text { get; }
	}
}
