﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;

namespace ServiceEmulator.Admin.DataSupport
{
	public class SizeAdmin : IAdmin
	{
		public string Title => "Database Admin : Update Data Set Size";

		public string SettingDescription => _description ?? GetDescription();

		public int Setting { get; private set; } = 32;

		public IReadOnlyList<string> Text => null;

		public void UpdateSize()
		{
			this.ConsoleTitle();
			while (true)
			{
				Console.WriteLine(string.Format("Current Size: {0}", Setting));
				Console.WriteLine("Enter a new data set size");
				Console.WriteLine("or enter 0 to retain current size");
				var input = Console.ReadLine();
				if (Int32.TryParse(input, out int newSize))
				{
					if (newSize == 0)
					{
						return;
					}
					else if (newSize > 0)
					{
						Setting = newSize;
						_description = null;
						return;
					}
				}
				this.ConsoleTitle();
				Console.WriteLine(string.Format("Invalid value: {0}", input));
				Console.WriteLine("\nPlease enter a positive integer");
			}
		}

		public bool UpdateSetting(int newSetting)
        {
			return false;
        }

		private string GetDescription()
		{
			_description = $"{Setting}";
			return _description;
		}

		private string _description;
	}
}
