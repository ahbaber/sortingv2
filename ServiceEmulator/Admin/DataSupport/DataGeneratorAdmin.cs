﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using DataService;
using DataService.DataGenerators;
using System.Collections.Generic;

namespace ServiceEmulator.Admin.DataSupport
{
	public class DataGeneratorAdmin : IDataGeneratorCollection, IAdmin
	{
		public DataGeneratorAdmin(IDataGeneratorCollection dataGenerator)
		{
			_dataGeneratorCollection = dataGenerator;
		}

		public string Title => "Database Admin : Data Ordering Selection";

		public string SettingDescription => _text[Setting];

		public int Setting => _dataGeneratorCollection.Setting;

		public IReadOnlyList<string> Text => _text;

		public IDataGeneratorCollection AddDataGenerator(IDataGenerator dataGenerator)
		{
			_dataGeneratorCollection.AddDataGenerator(dataGenerator);
			_text.Add(dataGenerator.Name);
			return this;
		}

		public bool UpdateSetting(int newSetting)
		{
			return _dataGeneratorCollection.UpdateSetting(newSetting);
		}

		public List<int> GetData(int size)
		{
			return _dataGeneratorCollection.GetData(size);
		}

		private readonly List<string> _text = new List<string>();
		private readonly IDataGeneratorCollection _dataGeneratorCollection;
	}
}
