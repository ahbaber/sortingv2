﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using SortControl;
using Sorting.Algorithms;
using System;
using System.Collections.Generic;

namespace ServiceEmulator.Admin
{
	public class SorterAdmin : ISortController, IAdmin
	{
		public SorterAdmin(ISortController sortController)
		{
			_sortController = sortController;
		}

		public string Title => "Sort Selector";

		public string SettingDescription => _text[_sortController.Setting];

		public int Setting => _sortController.Setting;

		public IReadOnlyList<string> Text => _text;

		public bool UpdateSetting(int newSetting)
		{
			return _sortController.UpdateSetting(newSetting);
		}

		public ISortController AddSorter(ISortMode sorter)
		{
			_sortController.AddSorter(sorter);
			_text.Add(sorter.Name);
			return this;
		}

		public ISortController AddSorter(ISegmentedSortMode sorter)
		{
			if (_quickInsert == null)
			{
				_sortController.AddSorter(sorter);
				_quickInsert = sorter;
				_quickInsertIndex = _text.Count;
				_text.Add(sorter.Name);
			}
			return this;
		}

		public ISorter GetSorter()
		{
			return _sortController.GetSorter();
		}

		public void UpdateMinSegment()
		{
			if (_quickInsert == null) { return; }

			Console.WriteLine();
			while (true)
			{
				Console.Write("Enter a minimum segment length: ");
				var input = Console.ReadLine();
				if (Int32.TryParse(input, out int size)
				&& size > 0)
				{
					_quickInsert.SegmentSize = size;
					Console.WriteLine();
					_text[_quickInsertIndex] = _quickInsert.Name;
					return;
				}
				Console.WriteLine("\nPlease enter a positive integer");
			}
		}

		private ISegmentedSortMode _quickInsert;
		private int _quickInsertIndex = -1;
		private readonly List<string> _text = new List<string>();
		private readonly ISortController _sortController;
	}
}
