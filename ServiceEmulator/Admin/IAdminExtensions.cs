﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;

namespace ServiceEmulator.Admin
{
	public static class IAdminExtensions
	{
		public static void ConsoleTitle(this IAdmin admin, bool isError = false, string command = "")
		{
			Console.Clear();
			Console.WriteLine(admin.Title);
			if (isError)
			{
                Console.WriteLine($"Invalid Selection: {command}");
                Console.WriteLine("Please select one of the following:");
			}
		}

		public static bool SelectionList(this IAdmin admin)
		{
			int oldSetting = admin.Setting;
			admin.ConsoleTitle();
			while (true)
			{
				Console.WriteLine($"Current: {admin.SettingDescription}");

				for (int i = 0; i < admin.Text.Count; ++i)
				{
					Console.WriteLine($"  {i+1}: {admin.Text[i]}");
				}
				Console.WriteLine("  0: Exit");

				bool isError = false;
				string input = Console.ReadKey(true).KeyChar.ToString();
				if (Int32.TryParse(input, out int command))
				{
					if (command == 0)
					{ return admin.WasUpdated(oldSetting); }
					isError = !admin.UpdateSetting(command-1);
				}
				else
				{ isError = true; }

				admin.ConsoleTitle(isError, input);
			}
		}

		public static bool WasUpdated(this IAdmin admin, int oldSetting)
		{
			return oldSetting != admin.Setting;
		}
	}
}
