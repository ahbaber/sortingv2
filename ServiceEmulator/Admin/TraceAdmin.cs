﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using TraceControl;
using TraceControl.Factories;

namespace ServiceEmulator.Admin
{
	public class TraceAdmin : ITraceControl, IAdmin
	{
		public TraceAdmin(ITraceControl traceControl)
		{
			_traceControl = traceControl;
		}

		public string Title => "Trace Admin";

		public string SettingDescription => _text[_traceControl.Setting];

		public IReadOnlyList<string> Text => _text;

		public int Setting => _traceControl.Setting;

		public bool Process(int command)
		{
			return _traceControl.UpdateSetting(command);
		}

		public bool UpdateSetting(int newSetting)
		{
			return _traceControl.UpdateSetting(newSetting);
		}

		public ITraceControl AddTraceMode(ITraceMode traceMode)
		{
			_traceControl.AddTraceMode(traceMode);
			_text.Add(traceMode.Name);
			return this;
		}

		public MetaFactory GetFactories()
		{
			return _traceControl.GetFactories();
		}

		public ITraceControl RefreshFactories()
		{
			_traceControl.RefreshFactories();
			return this;
		}

		public string GetTraceData(string sortName, string dataOrder)
		{
			return _traceControl.GetTraceData(sortName, dataOrder);
		}

		private List<string> _text = new List<string>();
		private readonly ITraceControl _traceControl;
	}
}
