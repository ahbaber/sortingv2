﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using ServiceEmulator.Admin.DataSupport;
using Sorting.Algorithms;
using System;
using System.Collections.Generic;
using TraceControl;

namespace ServiceEmulator.Admin
{
	public class DataServiceAdmin : IAdmin
	{
		public DataServiceAdmin(DataGeneratorAdmin dataGeneratorAdmin, ITraceControl traceControl)
		{
			_dataGeneratorAdmin = dataGeneratorAdmin;
			_traceControl = traceControl;
		}

		public string Title => "Data Service Admin";

		public string SettingDescription => _description ?? GetDescription();

		public int Setting => 0;

		public int MinSegment { get; set; } = 8;

		public IReadOnlyList<string> Text => _text;

		public bool UpdateSetting(int command)
		{
			switch (command)
			{
				case 0:
					if (_dataGeneratorAdmin.SelectionList())
					{
						_data = null;
						_description = null;
					}
					return true;
				case 1:
					int oldSize = _sizeAdmin.Setting;
					_sizeAdmin.UpdateSize();
					if (_sizeAdmin.WasUpdated(oldSize))
					{
						_data = null;
						_description = null;
					}
					return true;
			}
			return false;
		}

		public DataSet GetData()
		{
			if (_data == null)
			{
				_data = _dataGeneratorAdmin.GetData(_sizeAdmin.Setting);
			}
			Console.WriteLine(string.Format("Data: {0}", SettingDescription));
			// copy _data so that we can keep using it to compare how different
			// sorts handle the same data.
			var copy = new List<int>(_data);
			return _traceControl.GetFactories().SetFactory.Construct(copy);
		}

		private string GetDescription()
		{
			_description = $"{_dataGeneratorAdmin.SettingDescription}({_sizeAdmin.SettingDescription})";
			return _description;
		}

		private string _description;
		private List<int> _data;
		private readonly List<string> _text = new List<string>
		{
			"Change Data Ordering",
			"Change Data Set Size",
		};

		private readonly DataGeneratorAdmin _dataGeneratorAdmin;
		private readonly ITraceControl _traceControl;
		private readonly SizeAdmin _sizeAdmin = new SizeAdmin();
	}
}
