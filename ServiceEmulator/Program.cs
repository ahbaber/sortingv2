﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using DataService;
using DataService.DataGenerators;
using ServiceEmulator.Admin;
using ServiceEmulator.Admin.DataSupport;
using SortControl;
using SortControl.SortModes;
using System;
using System.Diagnostics;
using TraceControl;
using TraceControl.TraceModes;

namespace ServiceEmulator
{
	class Program
	{
		static void Main()
		{
			TraceAdmin traceAdmin = new TraceAdmin(new TraceController());
			traceAdmin.AddTraceMode(new NoTrace())
				.AddTraceMode(new XmlTrace())
				.AddTraceMode(new XmlSmallTrace())
				.AddTraceMode(new HtmlTrace())
				.AddTraceMode(new CountTrace())
				.AddTraceMode(new CountDataTrace())
				.AddTraceMode(new XmlSmallDataTrace());

			traceAdmin.UpdateSetting(4); // setting count trace as default

			DataGeneratorAdmin dataGeneratorAdmin = new DataGeneratorAdmin(new DataGeneratorCollection());
			dataGeneratorAdmin.AddDataGenerator(new RandomDG())
				.AddDataGenerator(new Ordered())
				.AddDataGenerator(new Reverse())
				.AddDataGenerator(new Repeated())
				.AddDataGenerator(new QuickSortWorstCase());

			var database = new DataServiceAdmin(dataGeneratorAdmin, traceAdmin);

			var sorterAdmin = new SorterAdmin(new SortCollection(traceAdmin));
			sorterAdmin.AddSorter(new Simple())
				.AddSorter(new Selection())
				.AddSorter(new Insert())
				.AddSorter(new Quick())
				.AddSorter(new QuickInsert())
				.AddSorter(new Heap());

			bool keepGoing = true;
			while (keepGoing)
			{
				Console.WriteLine($"Data  : {database.SettingDescription}");
				Console.WriteLine($"Trace : {traceAdmin.SettingDescription}");
				Console.WriteLine("Select Command");
				int last = Math.Min(6, sorterAdmin.Text.Count);
				for (int i = 0; i < last; ++i)
				{
					Console.WriteLine($"  {i + 1}: {sorterAdmin.Text[i]}");
				}
				Console.WriteLine("  7: Set Quick/Insert Min Segment Size");
				Console.WriteLine("-------------------");
				Console.WriteLine("  8: Database Admin");
				Console.WriteLine("  9: System Admin");
				Console.WriteLine("  0: Exit");

				bool isError = false;
				string input = Console.ReadKey(true).KeyChar.ToString();
				Console.WriteLine();

				if (Int32.TryParse(input, out int command))
				{
					switch (command)
					{
						case 0:
							keepGoing = false;
							break;
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
							if (sorterAdmin.UpdateSetting(command - 1))
							{
								Console.WriteLine($"Executing {sorterAdmin.SettingDescription}");
								traceAdmin.RefreshFactories();
								var data = database.GetData();
								var sort = sorterAdmin.GetSorter();
								data.PreCheckData();
								var timer = Stopwatch.StartNew();
								sort.Sort(data);
								timer.Stop();
								int outOfOrder = data.ResponseData();
								if (outOfOrder == 0)
								{ Console.WriteLine("All data is in order"); }
								else
								{
									Console.WriteLine($"Error: {outOfOrder} elements out of order");
								}
								Console.WriteLine(traceAdmin.GetTraceData(sorterAdmin.SettingDescription, database.SettingDescription));
								Console.WriteLine($"Elapsed: {timer.ElapsedMilliseconds}ms");
								Console.WriteLine();
							}
							else
							{ isError = true; }
							break;
						case 7:
							sorterAdmin.UpdateMinSegment();
							break;
						case 8:
							database.SelectionList();
							Console.Clear();
							break;
						case 9:
							traceAdmin.SelectionList();
							Console.Clear();
							break;
						default:
							isError = true;
							break;
					}
				}
				else
				{ isError = true; }
				if (isError)
				{
					Console.Clear();
					Console.WriteLine(string.Format("Invalid Selection: {0}", input));
					Console.WriteLine("Please select one of the following:");
				}
			}
			Console.WriteLine("done");
		}
	}
}
