﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl
{
	public interface ITraceMode
	{
		string Name { get; }
		ITracer Tracer { get; }
		NodeFormatter Formatter { get; }
		string FileType { get; }
		MetaFactory Factories { get; }
		string GetTraceData(string sortName, string dataOrder);
	}
}
