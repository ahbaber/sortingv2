﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using TraceControl.Factories;

namespace TraceControl
{
	public class TraceController : ITraceControl
	{
		public int Setting { get; private set; }

		public bool UpdateSetting(int newSetting)
		{
			if (0 <= newSetting && newSetting < _modes.Count)
			{
				Setting = newSetting;
				RefreshFactories();
				return true;
			}
			return false;
		}

		public ITraceControl AddTraceMode(ITraceMode traceMode)
		{
			_modes.Add(traceMode);
			RefreshFactories();
			return this;
		}

		public MetaFactory GetFactories()
		{
			if (_metaFactory == null)
			{ _metaFactory = _modes[Setting].Factories; }

			return _metaFactory;
		}

		public ITraceControl RefreshFactories()
		{
			_metaFactory = null;
			return this;
		}

		public string GetTraceData(string sortName, string dataOrder)
		{
			return _modes[Setting].GetTraceData(sortName, dataOrder);
		}

		private List<ITraceMode> _modes = new List<ITraceMode>();
		private MetaFactory _metaFactory = null;
	}
}
