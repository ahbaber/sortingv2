﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class NoTrace : ITraceMode
	{
		public string Name => "None";

		public ITracer Tracer => null;

		public NodeFormatter Formatter => null;

		public string FileType => null;

		public MetaFactory Factories { get; } = MetaFactory.CreateMetaFactory();

		public string GetTraceData(string sortName, string dataOrder)
		{
			return string.Empty;
		}
	}
}
