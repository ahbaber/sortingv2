﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.IO;
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public abstract class AbstractWriteFile : ITraceMode
	{
		public abstract string Name { get; }

		public ITracer Tracer { get; protected set; }

		public abstract NodeFormatter Formatter { get; }

		public abstract string FileType { get; }

		public abstract MetaFactory Factories { get; }

		public string GetTraceData(string sortName, string dataOrder)
		{
			var now = DateTime.Now;
			string dir = Directory.GetCurrentDirectory();
			string projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
			int index = dir.IndexOf(projectName, StringComparison.InvariantCulture);
			dir = dir.Remove(index); // now at solution root folder

			dir = Path.Combine(dir, "Output");
			Directory.CreateDirectory(dir);

			string filename = now.ToString("yyyyMMdd_HHmmss") + "_" + sortName + dataOrder + FileType;
			dir = Path.Combine(dir, filename);

			string text = Formatter.FormatSafe((Tracer as Tracer).Root);
			using (StreamWriter writer = new StreamWriter(dir))
			{
			    writer.WriteLine(text);
			}
			return "Written to file: " + dir;
		}
	}
}
