﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Formatters;
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class XmlSmallDataTrace : AbstractWriteFile
	{
		public override string Name => "XML Small Data";

		public override NodeFormatter Formatter { get; } = new XmlSmallNodeFormatter()
			.SetTab("\t")
			.SetNewLine("\n");

		public override string FileType => ".xml";

		public override MetaFactory Factories
		{
			get
			{
				Tracer = new Tracer("Root");
				return DataSetTraceMetaFactory.CreateMetaFactory(Tracer);
			}
		}
	}
}
