﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class CountTrace : ITraceMode
	{
		public string Name => "Count";

		public ITracer Tracer { get; private set; }

		public NodeFormatter Formatter => null;

		public string FileType => null;

		public MetaFactory Factories
		{
			get
			{
				Tracer = new CountTracerFormatted();
				return TraceMetaFactory.CreateMetaFactory(Tracer);
			}
		}

		public string GetTraceData(string sortName, string dataOrder)
		{
			return Tracer.ToString();
		}
	}
}
