﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class CountDataTrace : ITraceMode
	{
		public string Name => "Count Data";

		public ITracer Tracer { get; private set; }

		public NodeFormatter Formatter => null;

		public string FileType => null;

		public MetaFactory Factories
		{
			get
			{
				Tracer = new CountTracerFormatted();
				return DataSetTraceMetaFactory.CreateMetaFactory(Tracer);
			}
		}

		public string GetTraceData(string sortName, string dataOrder)
		{
			return Tracer.ToString();
		}
	}
}
