﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using System.Linq;

namespace TraceControl.TraceModes
{
    public class CountTracerFormatted : CountTracer
    {
        public override string ToString()
        {
			int swaps = 0;
			if (Calls.ContainsKey("DataSet.Swap"))
			{
				//"DataSet.Swap" may not exist if there were no swaps
				// e.g. when sorting an ordered list
				swaps = Calls["DataSet.Swap"];
			}
			return string.Format("\nMethod Calls: {0}\nReads       : {1}\nSwaps       : {2}\nDepth       : {3}",
                Calls.Sum(c => c.Value),
                Reads.Sum(r => r.Value),
                swaps,
                MaxDepth);
        }
    }
}
