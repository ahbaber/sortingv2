﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Formatters;
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class HtmlTrace : AbstractWriteFile
	{
		public override string Name => "HTML";

		public override NodeFormatter Formatter { get; } = new HtmlNodeFormatter()
			.SetTab("\t")
			.SetNewLine("\n");

		public override string FileType => ".html";

		public override MetaFactory Factories
		{
			get
			{
				Tracer = new Tracer("Root");
				return TraceMetaFactory.CreateMetaFactory(Tracer);
			}
		}
	}
}
