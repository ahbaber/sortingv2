﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using TraceControl.Factories;

namespace TraceControl.TraceModes
{
	public class XmlTrace : AbstractWriteFile
	{
		public override string Name => "XML";

		public override NodeFormatter Formatter { get; } = new NodeFormatter()
			.SetTab("\t")
			.SetNewLine("\n");

		public override string FileType => ".xml";

		public override MetaFactory Factories
		{
			get
			{
				Tracer = new Tracer("Root");
				return TraceMetaFactory.CreateMetaFactory(Tracer);
			}
		}
	}
}
