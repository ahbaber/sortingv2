﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TraceControl.Factories;

namespace TraceControl
{
	public interface ITraceControl
	{
		int Setting { get; }
		bool UpdateSetting(int newSetting);
		ITraceControl AddTraceMode(ITraceMode traceMode);
		MetaFactory GetFactories();
		ITraceControl RefreshFactories();
		string GetTraceData(string sortName, string dataOrder);
	}
}
