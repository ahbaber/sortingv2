﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Factories;
using Traceability;
using TraceSorting.Factories;

namespace TraceControl.Factories
{
	public class DataSetTraceMetaFactory : MetaFactory
	{
		public static MetaFactory CreateMetaFactory(ITracer tracer)
		{
			var simple = new SimpleSortFactory();
			var selection = new SelectionSortFactory();
			var insert = new InsertSortFactory();
			var quick = new QuickSortFactory();
			var quickInsert = new QuickInsertSortFactory(insert); // note reuse of insert factory here
			var heap = new HeapSortFactory();
			var set = new TraceDataSetFactory(tracer);
			return new DataSetTraceMetaFactory(tracer, simple, selection, insert, quick, quickInsert, heap, set);
		}

		protected DataSetTraceMetaFactory(ITracer tracer, SimpleSortFactory simple, SelectionSortFactory selection, InsertSortFactory insert,
			QuickSortFactory quick, QuickInsertSortFactory quickInsert, HeapSortFactory heap, TraceDataSetFactory set)
		: base(simple, selection, insert, quick, quickInsert, heap, set)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
