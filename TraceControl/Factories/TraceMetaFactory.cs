﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using TraceSorting.Factories;

namespace TraceControl.Factories
{
    // the only changes made are adding the ITracer, and calling trace factories.
    public class TraceMetaFactory : MetaFactory
    {
        private readonly ITracer _tracer;

        public static MetaFactory CreateMetaFactory(ITracer tracer)
        {
            var simple = new TraceSimpleSortFactory(tracer);
            var selection = new TraceSelectionSortFactory(tracer);
            var insert = new TraceInsertSortFactory(tracer);
            var quick = new TraceQuickSortFactory(tracer);
            var quickInsert = new TraceQuickInsertSortFactory(tracer, insert); // note reuse of insert factory here
            var heap = new TraceHeapSortFactory(tracer);
            var set = new TraceDataSetFactory(tracer);
            return new TraceMetaFactory(tracer, simple, selection, insert, quick, quickInsert, heap, set);
        }

        protected TraceMetaFactory(ITracer tracer, TraceSimpleSortFactory simple, TraceSelectionSortFactory selection,
            TraceInsertSortFactory insert, TraceQuickSortFactory quick, TraceQuickInsertSortFactory quickInsert,
            TraceHeapSortFactory heap, TraceDataSetFactory set)
        :base(simple, selection, insert, quick, quickInsert, heap, set)
        {
            _tracer = tracer;
        }
    }
}
