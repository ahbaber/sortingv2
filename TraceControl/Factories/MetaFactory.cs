﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Factories;

namespace TraceControl.Factories
{
    public class MetaFactory
    {
        public SimpleSortFactory SimpleSortFactory { get; }
        public SelectionSortFactory SelectionSortFactory { get; }
        public InsertSortFactory InsertSortFactory { get; }
        public QuickSortFactory QuickSortFactory { get; }
        public QuickInsertSortFactory QuickInsertSortFactory { get; }
        public HeapSortFactory HeapSortFactory { get; }
        public DataSetFactory SetFactory { get; }

        public static MetaFactory CreateMetaFactory()
        {
            var simple = new SimpleSortFactory();
            var selection = new SelectionSortFactory();
            var insert = new InsertSortFactory();
            var quick = new QuickSortFactory();
            var quickInsert = new QuickInsertSortFactory(insert); // note reuse of insert factory here
            var heap = new HeapSortFactory();
            var set = new DataSetFactory();
            return new MetaFactory(simple, selection, insert, quick, quickInsert, heap, set);
        }

        protected MetaFactory(SimpleSortFactory simple, SelectionSortFactory selection, InsertSortFactory insert,
            QuickSortFactory quick, QuickInsertSortFactory quickInsert, HeapSortFactory heap, DataSetFactory set)
        {
            SimpleSortFactory = simple;
            SelectionSortFactory = selection;
            InsertSortFactory = insert;
            QuickSortFactory = quick;
            QuickInsertSortFactory = quickInsert;
            HeapSortFactory = heap;
            SetFactory = set;
        }
    }
}
